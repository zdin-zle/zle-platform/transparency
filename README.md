# Transparency

## Overview

Transparency is a module of the ZLE Platform designed to manage research transparency elements, including article summaries, project documentation, educational resources, and collaborative forums. It integrates with the ZLE ecosystem to centralize research knowledge sharing.

## Running Transparency as Part of the ZLE Platform

### Setup Environment Variables

Create an environment file `.env` in the respective directory and define the required variables:

```env
TRANSPARENCY_DB_ADMIN=your_admin
TRANSPARENCY_DB_PASSWORD=your_password
TRANSPARENCY_SECRET_KEY=some_secret_key
```

### Retrieve and Deploy Docker

Download the production `docker-compose` file from the Git repository and place it in the directory. You can do this manually, via Git, or using `wget`:

```bash
wget https://gitlab.com/zdin-zle/zle-platform/transparency/-/raw/main/docker-compose.prod.yml
```

Build and start the containers:

```bash
sudo docker compose -f /home/dev/transparency/docker-compose.prod.yml --env-file /home/dev/transparency/.env  up -d
```

### Apply Database Migrations

```bash
sudo docker exec transparency_webapp python manage.py migrate
```

### Update the Static Files

```bash
sudo docker exec transparency_webapp python manage.py collectstatic --no-input
```

## Updating Transparency on the Server

If the production `docker-compose` file has been modified, download the updated version from the repository and replace the existing one:

```bash
wget https://gitlab.com/zdin-zle/zle-platform/transparency/-/raw/main/docker-compose.prod.yml
```

Stop the running container and remove the old image:

```bash
sudo docker rm <image-id>    
```

Rebuild and restart the containers:

```bash
sudo docker compose -f /home/dev/transparency/docker-compose.prod.yml --env-file /home/dev/transparency/.env  up -d
```

Apply database migrations:

```bash
sudo docker exec transparency_webapp python manage.py migrate
```

Collect static files:

```bash
sudo docker exec transparency_webapp python manage.py collectstatic --no-input
```

## Running Transparency Locally

### Installation Steps

1. Install Docker from [Docker's official website](https://www.docker.com).
2. Clone the repository:

   ```bash
   git clone https://gitlab.com/zdin-zle/zle-platform/transparency.git
   ```

3. Install the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

4. Create an `.env` file with the following environment variables:

   ```env
   TRANSPARENCY_DB_ADMIN=your_admin
   TRANSPARENCY_DB_PASSWORD=your_password
   TRANSPARENCY_SECRET_KEY=some_secret_key
   ```

5. Build and run the application:

   ```bash
   docker build -t transparency_webapp_image .
   ```
   ```bash
   docker compose up -d
   ```
   ```bash
   docker exec transparencywebapp python manage.py migrate
   ```

6. Access the application in your browser:
   **http://localhost:8000/transparency/**

## Development

### Making Model Changes

When updating a model, generate migration files:

```bash
cd transparency
```
```bash
python manage.py makemigrations
```

### Applying Migrations

Execute the following inside the `transparency_webapp` container:

```bash
python manage.py migrate
```

## Project Structure

### Applications

This project consists of four main modules (apps):

- **Article Summaries**
- **Project Summaries**
- **Educational Content**
- **Forum**

Each module is contained in a separate folder. When adding a new application, use the existing structure as a reference. The following files require customization:

- **`admin.py`** – Defines models that appear in the admin interface.
- **`apps.py`** – Defines the app name used in templates.
- **`forms.py`** – Contains form definitions for the app.
- **`models.py`** – Defines database models and their fields.
- **`urls.py`** – Maps URLs to views.
- **`views.py`** – Handles the logic and rendering for the app.

To add a new app, update `urls.py` in the `transparency` folder and add the app’s configuration file to the `INSTALLED_APPS` variable in `settings.py`.

The `main` folder contains core functionalities such as:

- Communication with the federated search (defined in `views.py`).
- Two methods that send search results from Transparency to the federated search.
- The `urls.py` file, which defines routes for federated search requests.
- Global variables and classes used across multiple modules.

### Templates

Templates are stored in the `templates` folder.

- The **base template** (`base.html`) and **home template** for Transparency are in the main `templates` folder.
- Templates for subpages (e.g., listings, forms) are in `templates/app_templates/`.
- Templates used by multiple apps follow a common structure, while app-specific templates include the app name in their filename.

New templates should follow this format:

```html
{% extends 'base.html' %}
{% load static %}

{% block content %}
<div>
    <!-- Your content here -->
</div>
{% endblock %}
```

### Static Files

- **Images**: Located in `static/images/transparency/`.
- **CSS & JavaScript**: Stored in `static/assets/transparency/`, with JavaScript files named according to the templates they are used in.
