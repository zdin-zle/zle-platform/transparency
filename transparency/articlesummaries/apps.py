from django.apps import AppConfig


class ArticlesummariesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'articlesummaries'
