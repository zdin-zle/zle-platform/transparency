# Generated by Django 4.1.3 on 2023-03-07 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articlesummaries', '0023_alter_articlesummary_area1_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articlesummary',
            name='area1',
            field=models.CharField(choices=[('Digitalization', 'Digitalization'), ('Hydrogen', 'Hydrogen'), ('MobilityandTraffic', 'Mobility and Traffic'), ('PowerGrids', 'Power Grids'), ('PowerStorage', 'Power Storage'), ('ResourceEfficiency', 'Resource Efficiency'), ('SectorLinkage', 'Sector Linkage'), ('SocietyIssues', 'Society Issues'), ('SystemAnalysis', 'System Analysis')], max_length=1024),
        ),
        migrations.AlterField(
            model_name='articlesummary',
            name='area2',
            field=models.CharField(blank=True, choices=[('Digitalization', 'Digitalization'), ('Hydrogen', 'Hydrogen'), ('MobilityandTraffic', 'Mobility and Traffic'), ('PowerGrids', 'Power Grids'), ('PowerStorage', 'Power Storage'), ('ResourceEfficiency', 'Resource Efficiency'), ('SectorLinkage', 'Sector Linkage'), ('SocietyIssues', 'Society Issues'), ('SystemAnalysis', 'System Analysis')], max_length=1024),
        ),
        migrations.AlterField(
            model_name='articlesummary',
            name='area3',
            field=models.CharField(blank=True, choices=[('Digitalization', 'Digitalization'), ('Hydrogen', 'Hydrogen'), ('MobilityandTraffic', 'Mobility and Traffic'), ('PowerGrids', 'Power Grids'), ('PowerStorage', 'Power Storage'), ('ResourceEfficiency', 'Resource Efficiency'), ('SectorLinkage', 'Sector Linkage'), ('SocietyIssues', 'Society Issues'), ('SystemAnalysis', 'System Analysis')], max_length=1024),
        ),
    ]
