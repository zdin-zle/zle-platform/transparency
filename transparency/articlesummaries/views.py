# import view classes
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q, Prefetch
from django.forms import inlineformset_factory
from django.db import transaction

import logging

from .models import ArticleSummary, ArticlePDF, ArticleImage
from .forms import ArticleSummaryForm, ArticleSummaryReadBibtex, ArticleImageForm, ArticlePDFForm
from .apps import ArticlesummariesConfig
from os.path import basename

from main.globalvariables import AREA_CHOICES, CREATE_BUTTON_TEXTS, NUMBER_OF_ELEMTS_ON_PAGE, VIEW_FULL_TEXTS, DELETE_TEXTS

from pybtex.database.input import bibtex
from urllib import parse

logger = logging.getLogger(__name__)

ArticleImageFormSet = inlineformset_factory(ArticleSummary, ArticleImage, form=ArticleImageForm, extra=1, can_delete=True)
ArticlePDFFormSet = inlineformset_factory(ArticleSummary, ArticlePDF, form=ArticlePDFForm, extra=1, can_delete=True)


#Article Summary home page
class ArticleSummaryHome(TemplateView):
    template_name = 'app_templates/app_home.html'
    text_search = 'In this section you can find and read summaries of articles and papers and their related abstracts. The summaries are ordered by area. Either select one of the areas listed below in order to get an overview of the area-specific summaries.'
    text_add = 'You can create a new article summary by clicking the button button below.'
    
    extra_context={
        'appname': ArticlesummariesConfig.name,
        'areas': AREA_CHOICES[1:],
        'header_search': 'Search for Brief Article Summaries',
        'text_search': text_search,
        'header_add': 'Add a Brief Article Summary',
        'text_add': text_add,
        'text_button': CREATE_BUTTON_TEXTS[ArticlesummariesConfig.name]
    }



# Create a Article Summary

class ArticleSummaryCreateView(CreateView):
    model = ArticleSummary
    form_class = ArticleSummaryForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name
    }

    def get_success_url(self):
        return reverse('articlesummaries_detail', kwargs={'slug': self.object.slug})
    
    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['images'] = ArticleImageFormSet(self.request.POST, self.request.FILES)
            data['pdfs'] = ArticlePDFFormSet(self.request.POST, self.request.FILES)
        else:
            data['images'] = ArticleImageFormSet()
            data['pdfs'] = ArticlePDFFormSet()
        return data
    
    def form_valid(self, form):
        context = self.get_context_data()
        images = context['images']
        pdfs = context['pdfs']
        logger.debug(f"PDF formset data: {pdfs.data}")
        logger.debug(f"PDF formset files: {self.request.FILES}")
        
        with transaction.atomic():
            self.object = form.save()
            if images.is_valid():
                images.instance = self.object
                images.save()
            if pdfs.is_valid():
                pdfs.instance = self.object
                pdfs.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        context = self.get_context_data()
        images = context['images']
        pdfs = context['pdfs']
        
        for formset in [images, pdfs]:
            if not formset.is_valid():
                for subform in formset:
                    for field, errors in subform.errors.items():
                        for error in errors:
                            logger.error(f"Form error: {field}: {error}")
                            form.add_error(None, f"{field}: {error}")
        
        return self.render_to_response(self.get_context_data(form=form))

class ArticleSummaryReadBibtexView(FormView):
    form_class = ArticleSummaryReadBibtex
    template_name = 'app_templates/articlesummaries_readbibtex.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name
    }

    def post(self, request, *args, **kwargs): 
        self.object = []
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        bibtex_file = request.FILES['file_field'].read()

        if form.is_valid():
            bibtex_file = str(bibtex_file).replace('\\n', '').replace('\\r', '')
            bibtex_data = bibtex.Parser().parse_string(bibtex_file)
            bibtex_entries = list(bibtex_data.entries.keys())

            if len(bibtex_entries) > 1:
                messages.info(request, 'Only upload BibTex-Files with just one entry!')
                return self.form_invalid(form)
            else:
                entryname = bibtex_entries[0]
                self.urlquery = 'title=' + bibtex_data.entries[entryname].fields['title'] \
                    + '&authors=' + str(bibtex_data.entries[entryname].persons['author'][0])
                return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return ("%s?" + self.urlquery) % reverse('articlesummaries_create')

class ArticleSummaryDeleteView(DeleteView):
    model = ArticleSummary
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name,
        'text_delete': DELETE_TEXTS[ArticlesummariesConfig.name]
    }

    def get_success_url(self):
        return reverse('articlesummaries_list')

class ArticleSummaryUpdateView(UpdateView):
    model = ArticleSummary
    form_class = ArticleSummaryForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ArticlesummariesConfig.name
    }

    def get_success_url(self):
        return reverse('articlesummaries_detail', kwargs={'slug': self.object.slug})

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['images'] = ArticleImageFormSet(self.request.POST, self.request.FILES, instance=self.object)
            data['pdfs'] = ArticlePDFFormSet(self.request.POST, self.request.FILES, instance=self.object)
        else:
            data['images'] = ArticleImageFormSet(instance=self.object)
            data['pdfs'] = ArticlePDFFormSet(instance=self.object)
        return data
    
    def form_valid(self, form):
        context = self.get_context_data()
        images = context['images']
        pdfs = context['pdfs']
        logger.debug(f"PDF formset data: {pdfs.data}")
        logger.debug(f"PDF formset files: {self.request.FILES}")
        
        with transaction.atomic():
            self.object = form.save()
            if images.is_valid():
                images.instance = self.object
                images.save()
            if pdfs.is_valid():
                pdfs.instance = self.object
                pdfs.save()
        return super().form_valid(form)

class ArticleSummaryDetailView(DetailView):
    model = ArticleSummary
    template_name = 'app_templates/detail.html'
    slug_field = 'slug'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['appname'] = ArticlesummariesConfig.name
        pdfs = self.object.pdfs.all()
        for pdf in pdfs:
            pdf.filename = basename(pdf.pdf.name) if pdf.pdf else ''
        context['pdfs'] = pdfs
        
        # Author profiles as a dictionary
        context['author_profiles'] = {
            'Sebastian Lehnhoff': 'https://dev-zle.offis.de/competence/profiles/15',
            'Luca Manzek': 'https://dev-zle.offis.de/competence/profiles/15',
            'Fernando Penaherrera Vaca': 'https://dev-zle.offis.de/competence/profiles/15',
            'Fernando Peñaherrera': 'https://dev-zle.offis.de/competence/profiles/15',            
            'Annika Ofenloch': 'https://dev-zle.offis.de/competence/profiles/15',
            'Frank Schuldt': 'https://dev-zle.offis.de/competence/profiles/14',
            'Jan Petznik': 'https://dev-zle.offis.de/competence/profiles/14',
            'Thomas Poppinga': 'https://dev-zle.offis.de/competence/profiles/14',
            'Astrid Nieße': 'https://dev-zle.offis.de/competence/profiles/12',
            'Stephan Ferenz': 'https://dev-zle.offis.de/competence/profiles/12',
            'Michael H. Breitner': 'https://dev-zle.offis.de/competence/profiles/11',
            'Michael H Breitner': 'https://dev-zle.offis.de/competence/profiles/11',
            'Sarah Eckhoff': 'https://dev-zle.offis.de/competence/profiles/11',
            'Sarah K. Lier': 'https://dev-zle.offis.de/competence/profiles/11',
            'Johannes Rolink': 'https://dev-zle.offis.de/competence/profiles/10',
            'Sarah Fayed': 'https://dev-zle.offis.de/competence/profiles/10',
            'Henrik Wagner': 'https://dev-zle.offis.de/competence/profiles/9',
            'Bernd Engel': 'https://dev-zle.offis.de/competence/profiles/9',
        }

        context['author_list'] = self.object.authors.split(', ')
        
        # Prepare a dictionary with author names and URLs
        context['author_profile_urls'] = {
            author: context['author_profiles'].get(author, None)
            for author in context['author_list']
        }

        return context
    
        

    
    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            Prefetch('pdfs', queryset=ArticlePDF.objects.all()),
            Prefetch('images', queryset=ArticleImage.objects.all())
        )

class ArticleSummaryListView(ListView):
    model = ArticleSummary
    template_name = 'app_templates/list.html'
    ordering = ['-pub_year']
    paginate_by = NUMBER_OF_ELEMTS_ON_PAGE
    extra_context = {
        'appname': ArticlesummariesConfig.name,
        'areas': AREA_CHOICES[1:],
        'text_button': CREATE_BUTTON_TEXTS[ArticlesummariesConfig.name],
        'text_viewfull': VIEW_FULL_TEXTS[ArticlesummariesConfig.name],
    }

    def get_context_data(self, **kwargs):
        # add extra variables for the template
        data = super().get_context_data(**kwargs)

        # get selected area from url query
        data['url_area'] = self.request.GET.get('area')

        # get total number of article summaries for paginator
        data['num_entries'] = self.get_queryset().count()
        data['num_all_entries'] = super().get_queryset().count()
        
        # get number of post with areas
        num_entries_area = []
        for area in AREA_CHOICES:
            if area[0] != '':
                mytuple = (area[0], area[1], super().get_queryset().filter(Q(area1=area[0]) | Q(area2=area[0]) | Q(area3=area[0])).count())
                num_entries_area.append(mytuple)
        data['num_entries_area'] = num_entries_area
        
        return data

    def get_queryset(self):
        area = self.request.GET.get('area')
        q = super().get_queryset().prefetch_related(
            Prefetch('pdfs', queryset=ArticlePDF.objects.filter(pdf__isnull=False).exclude(pdf=''))
        )

        if area == 'all':
            return q
        else:
            return q.filter(Q(area1=area) | Q(area2=area) | Q(area3=area))
        
    def dispatch(self, *args, **kwargs):
        if not self.request.GET.get('area'):
            # redirect to area=all if no area variable is in url query
            return HttpResponseRedirect(reverse('articlesummaries_list') + "?area=all")
        else:
            return super().dispatch(*args, **kwargs)