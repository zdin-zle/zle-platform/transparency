from django import forms
from django.core.exceptions import ValidationError

from .models import EducationalContentCourse, EducationalContentLecture
from main.globalvariables import AREA_CHOICES, DIFFICULTY_LEVELS


# form for the courses
class EducationalContentForm(forms.ModelForm):
    class Meta:
        model = EducationalContentCourse
        exclude = ('slug',) # add user here sometime
        
        # specify field properties
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-object'}),
            'institutions': forms.TextInput(attrs={'class': 'form-object'}),
            'department': forms.TextInput(attrs={'class': 'form-object'}),
            'authors': forms.TextInput(attrs={'class': 'form-object'}),
            'pub_year': forms.NumberInput(attrs={'class': 'form-object'}),

            'area1': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(1, this.value)'}),
            'area2': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(2, this.value)'}),
            'area3': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(3, this.value)'}),

            'difficulty': forms.Select(choices=DIFFICULTY_LEVELS, attrs={'class': 'form-select'}),

            'description': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 1500 characters', 'rows': 6}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # specify individual labels
        self.fields['title'].label = 'Title * '
        self.fields['institutions'].label = 'Institution * '
        self.fields['authors'].label = 'Lecturer(s) * '
        self.fields['pub_year'].label = 'Year of original Publication * '

        self.fields['area1'].label = 'Choose an area * '
        self.fields['area2'].label = 'Choose a second area'
        self.fields['area3'].label = 'Choose a third area'
        
        self.fields['difficulty'].label = 'Difficulty * '
        
        self.fields['description'].label = 'Description * '


# form for the lectures
class EducationalContentLectureForm(forms.ModelForm):
    class Meta:
        model = EducationalContentLecture
        exclude = ()

        # specify field properties
        widgets = {
            'educationalcontent': forms.HiddenInput, # hide foreignkey

            'title': forms.TextInput(attrs={'class': 'form-object'}),
            'url': forms.URLInput(attrs={'class': 'form-object'}),
            'file': forms.FileInput(attrs={'class': 'form-file-upload'}),
            'description': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 1500 characters', 'rows': 6})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # specify individual labels
        self.fields['educationalcontent'].label = 'Select course'
        self.fields['title'].label = 'Lecture Title * '
        self.fields['url'].label = 'Lecture URL (e.g. to a video)'
        self.fields['description'].label = 'Lecture Description * '


    def clean(self):
        cleaned_data = super().clean()

        # check that either an url was entered or a PDF was uploaded
        if not (cleaned_data['url'] or cleaned_data['file']):
            raise ValidationError('Either upload a PDF or specify an URL for this lecture.')
