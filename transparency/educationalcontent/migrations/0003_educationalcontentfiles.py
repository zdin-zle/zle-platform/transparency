# Generated by Django 4.2 on 2023-04-26 10:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('educationalcontent', '0002_rename_abstract_educationalcontent_description_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='EducationalContentFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(blank=True, upload_to='fileseducationalcontent')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='educationalcontent.educationalcontent')),
            ],
        ),
    ]
