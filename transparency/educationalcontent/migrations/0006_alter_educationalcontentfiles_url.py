# Generated by Django 4.2 on 2023-04-26 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('educationalcontent', '0005_rename_post_educationalcontentfiles_educationalcontent_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='educationalcontentfiles',
            name='url',
            field=models.URLField(blank=True),
        ),
    ]
