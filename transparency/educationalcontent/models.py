from django.db import models
from django_extensions.db.fields import AutoSlugField
from django.core.validators import MaxValueValidator, MinValueValidator, FileExtensionValidator

from main.globalvariables import AREA_CHOICES, DIFFICULTY_LEVELS, FORM_MIN_YEAR, FORM_MAX_YEAR, FORM_MAX_IMAGE_SIZE
from main.globalclasses import FileSizeValidator
from .apps import EducationalcontentConfig


# model for the a course
class EducationalContentCourse(models.Model):
    # initialise fields
    title = models.CharField(max_length=1024)
    institutions = models.CharField(max_length=1024)
    department = models.CharField(max_length=1024, blank=True)
    authors = models.CharField(max_length=1024)
    pub_year  = models.PositiveIntegerField(validators=[MinValueValidator(FORM_MIN_YEAR), MaxValueValidator(FORM_MAX_YEAR)])

    area1 = models.CharField(max_length=1024, choices=AREA_CHOICES)
    area2 = models.CharField(max_length=1024, choices=AREA_CHOICES, blank=True)
    area3 = models.CharField(max_length=1024, choices=AREA_CHOICES, blank=True)

    difficulty = models.CharField(max_length=1024, choices=DIFFICULTY_LEVELS)

    description = models.CharField(max_length=1500)

    slug = AutoSlugField(populate_from=['title'])
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True)

    def __str__(self):
        # specify the return text of the str-operator on this class
        return self.title


# model for the lectures to a course
class EducationalContentLecture(models.Model):
    # foreign key to EducationalContentCourse
    educationalcontent = models.ForeignKey(EducationalContentCourse, related_name='lectures', on_delete=models.CASCADE)

    # initialise fields
    title = models.CharField(max_length=1024)
    file = models.FileField(blank=True, upload_to='files/'+EducationalcontentConfig.name, validators=[FileSizeValidator(FORM_MAX_IMAGE_SIZE), FileExtensionValidator(['pdf'])])
    url = models.URLField(blank=True) # add validator later
    description = models.CharField(max_length=1500)

