from django.urls import path
from .views import *


# define urls of sub-sites
urlpatterns = [
    # home
    path('', EducationalContentHome.as_view(), name="educationalcontent_home"),
    

    # course
    path('create/', EducationalContentCreateCourseView.as_view(), name="educationalcontent_create"),
    path('<slug:slug>/delete/', EducationalContentDeleteCourseView.as_view(), name="educationalcontent_delete"),
    path('<slug:slug>/update/', EducationalContentUpdateCourseView.as_view(), name="educationalcontent_update"),
    
    # lecture
    path('<slug:slug>/createlecture/', EducationalContentCreateLectureView.as_view(), name="educationalcontent_createlecture"),
    path('<slug:slug>/<int:id>/deletelecture/', EducationalContentDeleteLectureView.as_view(), name="educationalcontent_deletelecture"),
    path('<slug:slug>/<int:id>/updatelecture/', EducationalContentUpdateLectureView.as_view(), name="educationalcontent_updatelecture"),

    # general
    path('list/', EducationalContentListView.as_view(), name="educationalcontent_list"),
    path('<slug:slug>/', EducationalContentDetailView.as_view(), name="educationalcontent_detail"),
]
