from django.contrib import admin
from .models import ForumPost, ForumAnswer

# Register your models here.
class ForumAdmin(admin.ModelAdmin):
    pass
admin.site.register([ForumPost, ForumAnswer], ForumAdmin)