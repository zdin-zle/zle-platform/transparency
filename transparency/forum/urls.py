from django.urls import path
from .views import *


# define urls of sub-sites
urlpatterns = [
    # post
    path('create/', ForumCreatePostView.as_view(), name="forum_create"),
    path('<slug:slug>/delete/', ForumDeletePostView.as_view(), name="forum_delete"),
    path('<slug:slug>/update/', ForumUpdatePostView.as_view(), name="forum_update"),

    # answer
    path('<slug:slug>/createanswer/', ForumCreateAnswerView.as_view(), name="forum_createanswer"),
    path('<slug:slug>/<int:id>/deleteanswer/', ForumDeleteAnswerView.as_view(), name="forum_deleteanswer"),
    path('<slug:slug>/<int:id>/updateanswer/', ForumUpdateAnswerView.as_view(), name="forum_updateanswer"),

    # general
    path('list/', ForumListView.as_view(), name="forum_list"),
    path('<slug:slug>/', ForumDetailMixView.as_view(), name="forum_detail"),
]
