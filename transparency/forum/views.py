# import view classes
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# import further django classes
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.db.models import Q
# import app-specific models, forms and appname
from .models import ForumPost, ForumAnswer
from .forms import ForumPostForm, ForumAnswerForm
from .apps import ForumConfig

# import global variables
from main.globalvariables import AREA_CHOICES, CREATE_BUTTON_TEXTS, NUMBER_OF_ELEMTS_ON_PAGE, VIEW_FULL_TEXTS, DELETE_TEXTS



# create a post
class ForumCreatePostView(CreateView):
    model = ForumPost
    form_class = ForumPostForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ForumConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of newly created post
        return reverse('forum_detail', kwargs={'slug': self.object.slug})
    

# delete a post
class ForumDeletePostView(DeleteView):
    model = ForumPost
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': ForumConfig.name,
        'text_delete': DELETE_TEXTS[ForumConfig.name]
    }

    def get_success_url(self):
        # redirect to listing of all posts
        return reverse('forum_list')


# update a post
class ForumUpdatePostView(UpdateView):
    model = ForumPost
    form_class = ForumPostForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ForumConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of updated post
        return reverse('forum_detail', kwargs={'slug': self.object.slug})
    

# create an answer
class ForumCreateAnswerView(CreateView): # nECESSARY ?
    model = ForumAnswer
    form_class = ForumAnswerForm
    template_name = 'app_templates/create_foreignKey.html'
    extra_context = {
        'appname': ForumConfig.name
    }

    def get_success_url(self):
        # redirect to detailview of post
        return reverse('forum_detail', kwargs={'slug': self.get_post_slug()})

    def get_initial(self):
        initial = super().get_initial()

        # select post where to add the content from url-slug
        post = ForumPost.objects.get(slug=self.get_post_slug())

        # set that post as initial to the form
        initial = {'forumpost': post}
        return initial
    
    def get_post_slug(self):
        # get post slug from url string 
        return self.request.get_full_path().split('/')[-3]


# delete an answer 
class ForumDeleteAnswerView(DeleteView):
    model = ForumAnswer
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': ForumConfig.name
    }

    def get_object(self):
        # specify that an answer object should get selected by its id
        return ForumAnswer.objects.get(id=self.get_answer_id())

    def get_success_url(self):
        # redirect to the detail view of post
        return reverse('forum_detail', kwargs={'slug': self.get_post_slug()})
    
    def get_post_slug(self):
        # get post slug from url string 
        return self.request.get_full_path().split('/')[-4]
    
    def get_answer_id(self):
        # get answer id from url string 
        return self.request.get_full_path().split('/')[-3]


# update an answer
class ForumUpdateAnswerView(UpdateView):
    model = ForumAnswer
    form_class = ForumAnswerForm
    template_name = 'app_templates/create_foreignKey.html'
    extra_context = {
        'appname': ForumConfig.name
    }

    def get_object(self):
        # specify that a answer object should get selected by its id
        return ForumAnswer.objects.get(id=self.get_answer_id())

    def get_success_url(self):
        # redirect to the detail view of post
        return reverse('forum_detail', kwargs={'slug': self.get_post_slug()})
    
    def get_post_slug(self):
        # get post slug from url string 
        return self.request.get_full_path().split('/')[-4]
    
    def get_answer_id(self):
        # get answer id from url string 
        return self.request.get_full_path().split('/')[-3]


# detail view for a post (not used -> see next class)
class ForumDetailView(DetailView): # neccesary ??
    model = ForumPost
    template_name = 'app_templates/detail_forum.html'
    slug_field = 'slug'
    
    def get_context_data(self,**kwargs):
        extra_context = super(ForumDetailView,self).get_context_data(**kwargs)
        extra_context['appname'] = ForumConfig.name

        # add all answers
        extra_context['answers'] = self.object.answers.all()

        return extra_context


# detail view of course with integrated answer-form
class ForumDetailMixView(FormMixin, DetailView):
    model = ForumPost
    form_class = ForumAnswerForm
    template_name = 'app_templates/detail_forum.html'

    def get_success_url(self):
        # open the post itself again when submitted an answer
        return reverse('forum_detail', kwargs={'slug': self.object.slug})

    def get_context_data(self, **kwargs):
        extra_context = super(ForumDetailMixView, self).get_context_data(**kwargs)

        extra_context['appname'] = ForumConfig.name

        # add answer-form to template with displayed post as initial
        extra_context['form'] = ForumAnswerForm(initial={'forumpost': self.object})

        # add all answers
        extra_context['answers'] = self.object.answers.all()

        return extra_context

    def post(self, request, *args, **kwargs):
        # check form is valid when submitted
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # save entered content from form when submitted
        form.save()
        return super(ForumDetailMixView, self).form_valid(form)


# listing of all posts
class ForumListView(ListView):
    model = ForumPost
    template_name = 'app_templates/list.html'
    ordering = ['-date_modified']
    paginate_by = NUMBER_OF_ELEMTS_ON_PAGE
    extra_context = {
        'appname': ForumConfig.name,
        'areas': AREA_CHOICES[1:],
        'text_button': CREATE_BUTTON_TEXTS[ForumConfig.name],
        'text_viewfull': VIEW_FULL_TEXTS[ForumConfig.name] # not necessary
    }

    def get_context_data(self, **kwargs):
        # add extra variables for the template
        extra_context = super().get_context_data(**kwargs)
        
        # get selected area from url query
        extra_context['url_area'] = self.request.GET.get('area')
        
        # get total number of posts for paginator
        extra_context['num_entries'] = self.get_queryset().count()
        
        extra_context['num_all_entries'] = super().get_queryset().count()
        
        # get number of post with areas
        num_entries_area = []
        for area in AREA_CHOICES:
            if area[0] != '':
                mytuple = (area[0], area[1], super().get_queryset().filter(Q(area1=area[0]) | Q(area2=area[0]) | Q(area3=area[0])).count())
                num_entries_area.append(mytuple)
        extra_context['num_entries_area'] = num_entries_area
        
        return extra_context

    def get_queryset(self):
        # get selected area from url query
        area = self.request.GET.get('area')
        
        # get answers database
        q = super().get_queryset()
        
        # only display posts with selected area
        if area == 'all':
            return q
        else:
            return q.filter(area1=area) | q.filter(area2=area) | q.filter(area3=area)
        
    def dispatch(self, *args, **kwargs):
        if not self.request.GET.get('area'):
            # redirect to area=all if no area variable is in url query
            return HttpResponseRedirect(reverse('forum_list') + "?area=all")
        else:
            return super().dispatch(*args, **kwargs)
 