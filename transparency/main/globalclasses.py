# import django classes
from django.core.validators import BaseValidator
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _



# class for the model-validator of the uploaded file size
@deconstructible
class FileSizeValidator(BaseValidator):
    # error message if file size is too big
    message = _("File too large. Size should not exceed %(limit_value)s MB.")
    code = "max_value"

    def compare(self, imageField, maxFileSize):
        # check if uploaded file size is too big
        return imageField.size > maxFileSize * 1024 * 1024