from django.urls import path
from django.views.generic import TemplateView
from .views import *

# define urls of sub-sites
urlpatterns = [
    # main home website
    path('', TemplateView.as_view(template_name="transparency_home.html"), name="transparency_home"),

    # search API
    path("search/", send_searchresults, name="send_searchresults"),
    path("search/filters", send_filters, name="send_filters"),
]