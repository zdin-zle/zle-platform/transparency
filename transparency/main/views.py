# import django classes
from django.db.models import Q
from django.http import JsonResponse

# import further needer classes
from functools import reduce
from operator import or_

# import models from apps
from articlesummaries.models import ArticleSummary
from projectsummaries.models import ProjectSummary
from educationalcontent.models import EducationalContentCourse, EducationalContentLecture
from forum.models import ForumPost, ForumAnswer

# import global variables
from .globalvariables import SEARCH_FILTERS



# method to send information to the federated search about found objects in transparency 
def send_searchresults(request):
    # implement module/app the search:
    # 0. import the model of the module
    # 1. in globalvariables.py: SEARCH_FILTERS[Modules] add the module name
    # 2. define new function add_{moduleName} and define attributes that should get searched
    # 3. in the first if statement: call the defined add-function into the object_list-array
    # 4. in the first else statement: write another elif into the for-loop and call the add-function for this module in the append statement
    # 5. specify the desc-variable for the module for the json in the for-loop right before the return statement


    # get the search query
    query = request.GET.get('q')

    # access filter dictionary of transparnecy
    filter_list = list(SEARCH_FILTERS.keys())


    ### filter for module
    # get selected module-filters
    module_filter_options = request.GET.getlist(filter_list[0]) 
    module_filter_options = [s.replace(' ', '').lower() for s in module_filter_options]

    if not module_filter_options:
        # no module filter was applied
        # all found objects are added to the search results
        object_list = [
            add_ArticleSummary(query),
            add_ProjectSummary(query),
            add_EducationalContent(query),
            add_Forum(query)
        ]

        # set filter options to all modules manually for later use to create the json
        module_filter_options = [s.replace(' ', '').lower() for s in SEARCH_FILTERS[filter_list[0]]]
    else:
        # a module filter was applied
        # all found objects are added to search results from selected modules
        object_list = []
        for f in module_filter_options:
            if f == 'articlesummaries':
                object_list.append(add_ArticleSummary(query))
            elif f == 'projectsummaries':
                object_list.append(add_ProjectSummary(query))
            elif f == 'educationalcontent':
                object_list.append(add_EducationalContent(query))
            elif f == 'forum':
                object_list.append(add_Forum(query))


    # resulting object_list has for sub-lists for (one each app)
    # -> object_list = [
    #       [article summaries],
    #       [project summaries],
    #       [educational content courses],
    #       [forum posts]
    #    ]
        

    ### filter for area
    # get selected area-filters
    area_filter_options = request.GET.getlist(filter_list[1])

    # check if an area-filter was selected
    if area_filter_options:
        # use reduce() to iterate over all selected options of a specitic filter (e.g., 'areas')
        # create the filter string for the next step of removing objects
        filter_query = reduce(
            or_, (Q(**{f'area1__icontains': f}) | 
                  Q(**{f'area2__icontains': f}) | 
                  Q(**{f'area3__icontains': f}) for f in area_filter_options)
        )

        # remove all objects from the search results that are not in the selected areas
        for idx in range(len(object_list)):
            object_list[idx] = object_list[idx].filter(filter_query)

    ### make json for sending the search results
    # make object list to json format
    object_list_json = []

    # iterate over each app
    for idx, app_list in enumerate(object_list):
        # iterate over each object of an app
        for object in app_list:
            # specify desc-attribute for each app
            if module_filter_options[idx] == 'articlesummaries':
                desc = object.abstract
            elif module_filter_options[idx] == 'projectsummaries':
                desc = object.introduction
            elif module_filter_options[idx] == 'educationalcontent':
                desc = object.description
            elif module_filter_options[idx] == 'forum':
                desc = object.text

            # set areas of object as tags
            str_tags = object.get_area1_display()
            if object.area2:
                str_tags += ', ' + object.get_area2_display()
            if object.area3:
                str_tags += ', ' + object.get_area3_display()

            # create dictionary for each object with the entries for the evalutaion of the federated search
            object_json = {
                "id": object.id,
                "name": object.title,
                "element_name": "Transparency", # CHANGE: maybe write the app here as well ?
                "description": desc,
                "link": 'https://' + request.get_host() + '/transparency/' + module_filter_options[idx] + '/' + object.slug,
                "tags": str_tags
            }

            # add dictionary to results-list
            object_list_json.append(object_json)

    # send results-list to federated search
    return JsonResponse(object_list_json, safe=False)

def add_ArticleSummary(query):
    # CHANGE: which attributes should get searched here
    if query:
        return ArticleSummary.objects.filter(
            Q(title__icontains=query) | Q(authors__icontains=query) |
            Q(doi__icontains=query) | Q(pub_year__icontains=query) |
            Q(jour_conf__icontains=query) | Q(abstract__icontains=query) | 
            Q(summary__icontains=query)
        )
    else:
        return ArticleSummary.objects.all()

def add_ProjectSummary(query):
    # CHANGE: which attributes should get searched here
    if query:
        return ProjectSummary.objects.filter(
            Q(title__icontains=query) | Q(authors__icontains=query) |
            Q(institutions__icontains=query) | Q(url__icontains=query) | 
            Q(introduction__icontains=query) | Q(summary__icontains=query)
        )
    else:
        return ProjectSummary.objects.all()
    

def add_EducationalContent(query):
    # CHANGE: which attributes should get searched here
    # CHANGE: maybe search in the lectures as well ?
    if query:
        return EducationalContentCourse.objects.filter(
            Q(title__icontains=query) | Q(authors__icontains=query) |
            Q(institutions__icontains=query) | Q(department__icontains=query) | 
            Q(pub_year__icontains=query) | Q(description__icontains=query) |
            Q(difficulty__icontains=query)
        )
    else:
        return EducationalContentCourse.objects.all()
    
def add_Forum(query):
    # CHANGE: which attributes should get searched here
    # CHANGE: maybe search in the answers as well ?
    if query:
        return ForumPost.objects.filter(
            Q(title__icontains=query) | Q(authors__icontains=query) |
            Q(date_post__icontains=query) | Q(date_modified__icontains=query) | 
            Q(text__icontains=query)
        )
    else:
        return ForumPost.objects.all()


# method to send information to the federated search about the possible filters in transparency 
def send_filters(request):
    filters_response = []

    # iterate over all defined filters 
    for filtername in SEARCH_FILTERS.keys():
        # create list of dictionaries with the filtername and filteroptions
        filters_response.append(
            {
                'type': 'string_list',
                'field_name': filtername,
                'options': SEARCH_FILTERS[filtername]
            }
        )

    # send list of all filter-options for transparency
    return JsonResponse(filters_response, safe=False)
