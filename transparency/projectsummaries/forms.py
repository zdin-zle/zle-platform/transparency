from .models import ProjectSummary, ProjectImage, ProjectPDF
from django import forms
from main.globalvariables import AREA_CHOICES
from django.forms.models import inlineformset_factory


# form for the project summaries
class ProjectSummaryForm(forms.ModelForm):
    class Meta:
        model = ProjectSummary
        exclude = ('slug',) # add user here sometime
        
        # specify field properties
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-object'}),
            'institutions': forms.TextInput(attrs={'class': 'form-object'}),
            'authors': forms.TextInput(attrs={'class': 'form-object'}),
            'url': forms.URLInput(attrs={'class': 'form-object'}), 

            'area1': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(1, this.value)'}),
            'area2': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(2, this.value)'}),
            'area3': forms.Select(choices=AREA_CHOICES, attrs={'class': 'form-select', 'onchange': 'areaFieldChange(3, this.value)'}),

            'introduction': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 1500 characters', 'rows': 6}),
            'summary': forms.Textarea(attrs={'class': 'form-object', 'placeholder': 'Limited to 7000 characters', 'rows': 10}), 

            'image': forms.FileInput(attrs={'class': 'form-file-upload'}),
            'caption': forms.TextInput(attrs={'class': 'form-object', 'placeholder': 'Optional caption'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['institutions'].label = 'Participating Institutions * '

        # specify individual labels
        self.fields['title'].label = 'Title * '
        self.fields['url'].label = 'URL of Project Website'
        self.fields['authors'].label = 'Summary Author * '

        self.fields['area1'].label = 'Choose an area * '
        self.fields['area2'].label = 'Choose a second area'
        self.fields['area3'].label = 'Choose a third area'

        self.fields['introduction'].label = 'Short Introduction * '
        self.fields['summary'].label = 'Summary * '
        
    def clean(self):
        cleaned_data = super().clean()

        # delete paragraphs from copying from a PDF file
        cleaned_data['introduction'] = cleaned_data['introduction'].replace('\r', ' ').replace('\n', ' ')


class ProjectImageForm(forms.ModelForm):
    class Meta:
        model = ProjectImage
        fields = ('image', 'caption',)
        widgets = {
            'image': forms.FileInput(attrs={'class': 'form-file-upload'}),
            'caption': forms.TextInput(attrs={'class': 'form-object', 'placeholder': 'Optional caption'}),
        }

class ProjectPDFForm(forms.ModelForm):
    class Meta:
        model = ProjectPDF
        fields = ('pdf', 'description',)
        widgets = {
            'pdf': forms.FileInput(attrs={'class': 'form-file-upload'}),
            'description': forms.TextInput(attrs={'class': 'form-object', 'placeholder': 'Optional description'}),
        }

# Formsets for handling multiple image and PDF uploads

ProjectImageFormSet = inlineformset_factory(ProjectSummary, ProjectImage, form=ProjectImageForm, extra=1, can_delete=True)
ProjectPDFFormSet = inlineformset_factory(ProjectSummary, ProjectPDF, form=ProjectPDFForm, extra=1, can_delete=True)

