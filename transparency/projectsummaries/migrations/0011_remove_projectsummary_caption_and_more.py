# Generated by Django 5.0.7 on 2024-07-16 10:54

import django.core.validators
import django.db.models.deletion
import main.globalclasses
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projectsummaries", "0010_alter_projectsummary_id"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="projectsummary",
            name="caption",
        ),
        migrations.RemoveField(
            model_name="projectsummary",
            name="image",
        ),
        migrations.CreateModel(
            name="ProjectImage",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image",
                    models.ImageField(
                        blank=True,
                        null=True,
                        upload_to="images/projectsummaries",
                        validators=[main.globalclasses.FileSizeValidator(7)],
                    ),
                ),
                ("caption", models.CharField(blank=True, max_length=255)),
                (
                    "article",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="images",
                        to="projectsummaries.projectsummary",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ProjectPDF",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "pdf",
                    models.FileField(
                        blank=True,
                        null=True,
                        upload_to="pdfs/",
                        validators=[
                            django.core.validators.FileExtensionValidator(
                                allowed_extensions=["pdf"]
                            ),
                            main.globalclasses.FileSizeValidator(100),
                        ],
                    ),
                ),
                ("description", models.CharField(blank=True, max_length=255)),
                (
                    "article",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="pdfs",
                        to="projectsummaries.projectsummary",
                    ),
                ),
            ],
        ),
    ]
