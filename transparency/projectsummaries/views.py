# import view classes
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# import further django classes
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q, Prefetch
from django.forms import inlineformset_factory
from django.db import transaction

import logging

# import app-specific model, form and appname
from .models import ProjectSummary, ProjectImage, ProjectPDF
from .forms import ProjectSummaryForm, ProjectImageForm, ProjectPDFForm
from .apps import ProjectsummariesConfig

from os.path import basename
from pybtex.database.input import bibtex
from urllib import parse

# import global variables
from main.globalvariables import AREA_CHOICES, CREATE_BUTTON_TEXTS, NUMBER_OF_ELEMTS_ON_PAGE, VIEW_FULL_TEXTS, DELETE_TEXTS

logger = logging.getLogger(__name__)

ProjectImageFormSet = inlineformset_factory(ProjectSummary, ProjectImage, form=ProjectImageForm, extra=1, can_delete=True)
ProjectPDFFormSet = inlineformset_factory(ProjectSummary, ProjectPDF, form=ProjectPDFForm, extra=1, can_delete=True)


# app home page
class ProjectSummaryHome(TemplateView):
    template_name = 'app_templates/app_home.html'

    # declare strings on homepage
    text_search = 'In this section you can find and read summaries of different projects related to energy research. \
        The summaries are ordered by area. \
        Either select one of the areas listed below in order to get an overview of the area-specific  \
        summaries.'# or find a certain summary using the keyword-based search bar.'
    text_add = 'You can create a new project summary by clicking the button below.'

    extra_context={
        'appname': ProjectsummariesConfig.name,
        'areas': AREA_CHOICES[1:],
        'header_search': 'Search for Brief Project Summaries',
        'text_search': text_search,
        'header_add': 'Add a Brief Project Summaries',
        'text_add': text_add,
        'text_button': CREATE_BUTTON_TEXTS[ProjectsummariesConfig.name]
    }


# create a project summary
class ProjectSummaryCreateView(CreateView):
    model = ProjectSummary
    form_class = ProjectSummaryForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ProjectsummariesConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of newly created project summary
        return reverse('projectsummaries_detail', kwargs={'slug': self.object.slug}) 
    
    
    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['images'] = ProjectImageFormSet(self.request.POST, self.request.FILES)
            data['pdfs'] = ProjectPDFFormSet(self.request.POST, self.request.FILES)
        else:
            data['images'] = ProjectImageFormSet()
            data['pdfs'] = ProjectPDFFormSet()
        return data
    
    def form_valid(self, form):
        context = self.get_context_data()
        images = context['images']
        pdfs = context['pdfs']
        logger.debug(f"PDF formset data: {pdfs.data}")
        logger.debug(f"PDF formset files: {self.request.FILES}")
        
        with transaction.atomic():
            self.object = form.save()
            if images.is_valid():
                images.instance = self.object
                images.save()
            if pdfs.is_valid():
                pdfs.instance = self.object
                pdfs.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        context = self.get_context_data()
        images = context['images']
        pdfs = context['pdfs']
        
        for formset in [images, pdfs]:
            if not formset.is_valid():
                for subform in formset:
                    for field, errors in subform.errors.items():
                        for error in errors:
                            logger.error(f"Form error: {field}: {error}")
                            form.add_error(None, f"{field}: {error}")
        
        return self.render_to_response(self.get_context_data(form=form))

# delete a project summary
class ProjectSummaryDeleteView(DeleteView):
    model = ProjectSummary
    template_name = 'app_templates/delete.html'
    extra_context = {
        'appname': ProjectsummariesConfig.name,
        'text_delete': DELETE_TEXTS[ProjectsummariesConfig.name]
    }

    def get_success_url(self):
        # redirect to listing of all project summaries
        return reverse('projectsummaries_list')


# update a project summary
class ProjectSummaryUpdateView(UpdateView):
    model = ProjectSummary
    form_class = ProjectSummaryForm
    template_name = 'app_templates/create.html'
    extra_context = {
        'appname': ProjectsummariesConfig.name
    }

    def get_success_url(self):
        # redirect to detail view of updated project summary
        return reverse('projectsummaries_detail', kwargs={'slug': self.object.slug})
    
    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['images'] = ProjectImageFormSet(self.request.POST, self.request.FILES, instance=self.object)
            data['pdfs'] = ProjectPDFFormSet(self.request.POST, self.request.FILES, instance=self.object)
        else:
            data['images'] = ProjectImageFormSet(instance=self.object)
            data['pdfs'] = ProjectPDFFormSet(instance=self.object)
        return data
    
    def form_valid(self, form):
        context = self.get_context_data()
        images = context['images']
        pdfs = context['pdfs']
        logger.debug(f"PDF formset data: {pdfs.data}")
        logger.debug(f"PDF formset files: {self.request.FILES}")
        
        with transaction.atomic():
            self.object = form.save()
            if images.is_valid():
                images.instance = self.object
                images.save()
            if pdfs.is_valid():
                pdfs.instance = self.object
                pdfs.save()
        return super().form_valid(form)


# detail view for a project summary
class ProjectSummaryDetailView(DetailView):
    model = ProjectSummary
    template_name = 'app_templates/detail.html'
    slug_field = 'slug'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['appname'] = ProjectsummariesConfig.name
        pdfs = self.object.pdfs.all()
        for pdf in pdfs:
            pdf.filename = basename(pdf.pdf.name) if pdf.pdf else ''
        context['pdfs'] = pdfs
        return context
    
    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            Prefetch('pdfs', queryset=ProjectPDF.objects.all()),
            Prefetch('images', queryset=ProjectImage.objects.all())
        )


# listing of all project summaries
class ProjectSummaryListView(ListView):
    model = ProjectSummary
    template_name = 'app_templates/list.html'
    ordering = ['title']
    paginate_by = NUMBER_OF_ELEMTS_ON_PAGE
    extra_context = {
        'appname': ProjectsummariesConfig.name,
        'areas': AREA_CHOICES[1:],
        'text_button': CREATE_BUTTON_TEXTS[ProjectsummariesConfig.name],
        'text_viewfull': VIEW_FULL_TEXTS[ProjectsummariesConfig.name]
    }

    def get_context_data(self, **kwargs):
        # add extra variables for the template
        extra_context = super().get_context_data(**kwargs)
        
        # get selected area from url query
        extra_context['url_area'] = self.request.GET.get('area')
        
        # get total number of project summaries for paginator
        extra_context['num_entries'] = self.get_queryset().count()
        extra_context['num_all_entries'] = super().get_queryset().count()
        
        # get number of post with areas
        num_entries_area = []
        for area in AREA_CHOICES:
            if area[0] != '':
                mytuple = (area[0], area[1], super().get_queryset().filter(Q(area1=area[0]) | Q(area2=area[0]) | Q(area3=area[0])).count())
                num_entries_area.append(mytuple)
        extra_context['num_entries_area'] = num_entries_area
        
        return extra_context

    def get_queryset(self):
        # get selected area from url query
        area = self.request.GET.get('area')
        
        # get project summaries database
        q = super().get_queryset()
        
        # only display project summaries with selected area
        if area == 'all':
            return q
        else:
            return q.filter(area1=area) | q.filter(area2=area) | q.filter(area3=area)
        
    def dispatch(self, *args, **kwargs):
        if not self.request.GET.get('area'):
            # redirect to area=all if no area variable is in url query
            return HttpResponseRedirect(reverse('projectsummaries_list') + "?area=all")
        else:
            return super().dispatch(*args, **kwargs)