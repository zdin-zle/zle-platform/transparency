// save area-selects
let area1 = document.getElementById("add-entry").area1;
let area2 = document.getElementById("add-entry").area2;
let area3 = document.getElementById("add-entry").area3;

// initialise chosen select-options
var valueArea1 = area1.value;
var valueArea2 = area2.value;

// when opened disable select fields for area 2 and area 3 (when first area empty)
if (valueArea1 === "") {
    area2.disabled = "disbaled";
}
if (valueArea2 === "") {
    area3.disabled = "disbaled";
}

// disable already chosen options in following select fields (in edit mode)
disableSelectOptions();


// onchange event for select fields
function areaFieldChange(areaNum, chosenValue) {
    if (chosenValue === "") {
        // disable all next area-choices if default value again
        if (areaNum === 1) {
            valueArea1 = "";
            disableAreaSelect(2);
            disableAreaSelect(3);
        } else if (areaNum === 2) {
            valueArea2 = "";
            disableAreaSelect(3);
        }
    } else {
        // save chosen value and if duplicate change the latter option to default
        if (areaNum === 1) {
            valueArea1 = chosenValue;

            if (chosenValue === area2.value) {
                valueArea2 = "";
                disableAreaSelect(2);
                disableAreaSelect(3);
            } else if (chosenValue === area3.value) {
                disableAreaSelect(3);
            }
        } else if (areaNum === 2) {
            valueArea2 = chosenValue;

            if (chosenValue === area3.value) {
                disableAreaSelect(3);
            }
        }

        // enable and disable correct select-options
        disableSelectOptions();

        // enable next select
        area2.disabled = "";
        if (area2.value !== "") {
            area3.disabled = "";
        }
    }
}

// disable a specfic area select field
function disableAreaSelect(areaNum) {
    if (areaNum == 2) {
        area2.value = "";
        area2.disabled = "disabled";
    } else if (areaNum == 3) {
        area3.value = "";
        area3.disabled = "disabled";
    }
}

// disable specific (already chosen) options of the following select fields
function disableSelectOptions() {
    // enable all select-options
    area2.querySelectorAll("option").forEach(opt => {
        opt.disabled = "";
    });
    area3.querySelectorAll("option").forEach(opt => {
        opt.disabled = "";
    });

    // disable already chosen select-options
    if (valueArea1 !== "") {
        area2.querySelector("option[value="+valueArea1+"]").disabled="disabled";
        area3.querySelector("option[value="+valueArea1+"]").disabled="disabled";

        if (valueArea2 !== "") {
            area3.querySelector("option[value="+valueArea2+"]").disabled="disabled";
        }
    }
}

// save image upload and caption element
if ("{{ appname }}" !== "educationalcontent" && "{{ appname }}" !== "forum") {
    
}