var collapses = document.getElementsByClassName("modern-collapsible");

var i;
for (i = 0; i < collapses.length; i++) {
    collapses[i].addEventListener("click", async function() {
        this.classList.toggle("modern-collapsible-active");

        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
            //content.style.visibility = "hidden";
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
            content.style.visibility = "visible";
        } 
    });
}